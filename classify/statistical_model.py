from pytrie import SortedStringTrie as trie

class StatisticalModel:
    
  def __init__(self, files):
      self.class_num = len(files) # Number of classes
      self.files     = files      # List of files with tweets for each class
      self.tries     = []         # List of tries for each class
      self.stats     = []         # Anciliary statistics on number of tweets etc.

  def build(self):
    """
    Build tries and collect statistics
    """
    for fname in self.files:
      # Initialize structures
      s = {'tweet_num': 0,      # total number of tweets in class 
           'word_num': 0,       # total number of words in class
           'dist_word_num': 0}  # number of distinct words in class
      t = trie()
      self.stats.append(s)
      self.tries.append(t)

      # Read and process file
      f = open(fname,'r')
      for line in f:
        s['tweet_num'] += 1
        words = line.split(' ')

        for word in words:
          s['word_num'] += 1
          word = word.strip()

          try:
            val = t[word]
            val += 1
          except KeyError:
            val = 1
            s['dist_word_num'] += 1

          t.update({word:val})

  def _get_words(self, tweet):
    """
    Get the list of words out of tweet
    """
    words = tweet.split(' ')
    words = [word.strip() for word in words]
    return words

  def word_prob(self, word, cls_ind = None):
    """
    Calculate word probability condiitione or not on class.
    When class specified Laplace smoothing is used (if class specified).
    """
    if cls_ind is not None:
      assert self.class_num > cls_ind

      """
      P(word|class) = (1 + Count(word = word|class) / (k + Count(word|class))
      k = number of distinct words in class
      """
      t = self.tries[cls_ind]
      s = self.stats[cls_ind]

      # Number of times word occured in class 
      try:
        word_cnt_cls = t[word] 
      except KeyError:
        word_cnt_cls = 0

      # Total number of words in class
      word_cnt = s['word_num']

      # Number of distinct words
      k = s['dist_word_num']

      return float(1 + word_cnt_cls)/(k + word_cnt)

    else:
  
      """
      P(word) = Count(word = word) / Count(word) 
      """ 
      tot_words_num = 0
      word_cnt_num = 0      

      for i in range(self.class_num):
        s = self.stats[cls_ind]
        t = self.tries[cls_ind]
        tot_words_num += s['word_num']
        word_cnt_num += t[word]                 

      # FIXME: Laplace smoothing is not used
      return float(word_cnt_num)/tot_words_num

  def tweet_prob(self, tweet, cls_ind = None):
    """
    Calculate tweet probability conditioned or not on class.
    """
    if cls_ind is not None:
      assert self.class_num > cls_ind
 
      """
      P(tweet|class) = \prod_{word \in tweet} P(word|class) 
      """
      words = self._get_words(tweet)

      #cls_prob = self.cls_prob(cls_ind)
      wp = self.word_prob

      res = map(lambda x,y: wp(x,y),words,[cls_ind]*len(words))
      #res = [e*cls_prob for e in res]
      res = reduce(lambda x,y: x*y, res)

      return res

    else:
 
      """
      Calculate by full probability law.
      P(tweet) = \prod_{class} P(tweet|class) * P(class) 
      """
      cls_inds = range(0,self.class_num)

      tp = self.tweet_prob
      cp  = self.cls_prob

      res = map(lambda x,y: tp(y,x)*cp(x), cls_inds, [tweet]*len(cls_inds))

      return sum(res)

  def cls_prob(self, cls_ind, tweet = None):
    """
    Calculate the probability of class 
    conditioned or not on tweet.
    """
    assert self.class_num > cls_ind

    if tweet is None:

      """
      P(class) = Count(tweet|class) / Count(tweet)
      """
      # FIXME: assume that all tweets are different
      tweet_num_sum = 0
      for s in self.stats:
        tweet_num_sum += s['tweet_num'] 

      s = self.stats[cls_ind] 

      return float(s['tweet_num'])/tweet_num_sum

    else:

      """
      P(class|tweet) = ( P(class) * \prod_{word} P(word|class) ^ Count(word|tweet) ) / P(tweet) 
      """
      words = self._get_words(tweet)
      words_distinct = list(set(words))
      words_cnt = [words.count(word) for word in words_distinct]

      wp = self.word_prob
      res = map(lambda x,y,z: wp(x,y)**z, words_distinct, [cls_ind]*len(words_distinct), words_cnt)
      prod = reduce(lambda x,y: x*y, res)
      
      cls_prob = self.cls_prob(cls_ind)
      tweet_prob = self.tweet_prob(tweet)

      return float(cls_prob)*prod/tweet_prob 
       
if __name__ == '__main__':
  sm = StatisticalModel(['twitter_sentiment/train_pos.txt.out.orig','twitter_sentiment/train_neg.txt.out.orig'])
  sm.build()




