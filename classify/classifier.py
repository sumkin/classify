from statistical_model import StatisticalModel

class Classifier:

  def __init__(self,train_files,test_file):
    self.sm = StatisticalModel(train_files)
    self.test_file = test_file

  def learn(self):
    self.sm.build()

  def classify(self,tweet):
    sm = self.sm

    cls_num = sm.class_num
    probs = map(sm.cls_prob,range(cls_num),[tweet]*cls_num)
    return probs.index(max(probs))

  def test(self):

    tn = fp = fn = tp = 0

    f = open(self.test_file,'r')
    for line in f:
      cls_act,tweet = line.split('\t')
      # 0 = negative, 2 = neutral, 4 = positive
      cls_pre = self.classify(tweet)
      if cls_act.strip() == '2':
        continue
      if cls_act.strip() == '0':
        if cls_pre == 1:
          tn += 1
        else:
          fp += 1
      elif cls_act.strip() == '4':
        if cls_pre == 0:
          tp += 1
        else:
          fn += 1
    
    print 'Confusion matrix:'
    print tn,fp
    print fn,tp

    print 'Precision: ',float(tp)/(tp+fp)
    print 'Recall:',float(tp)/(tp+fn) 


if __name__ == '__main__':
  fpos  = 'twitter_sentiment/train_pos.txt.out.orig'
  fneg  = 'twitter_sentiment/train_neg.txt.out.orig'
  ftest = 'twitter_sentiment/test.txt.out'
  cl = Classifier([fpos,fneg],ftest)
  print 'Learning...'
  cl.learn()
  print 'Validating...'
  cl.test()    
      
