import re

emoticon_list = [r':\)',r':-\(',r':-\)',r':\(']

def replace_urls(s):
  # Replace urls in the string with URL
  return re.sub(r"http://\S*","URL",s)

def replace_emoticons(s):
  # Replace emoticons with empty string
  ans = s
  for e in emoticon_list:
    ans = re.sub(e,"",ans)  
  return ans

def replace_names(s):
  # Replace twitter usenames with USER
  return re.sub(r"@\S*","USER",s)

def replace_repeated(s):
  # Replace repeated more than two times letter with
  # two ones. E.g. fuuuuuun -> fuun
  return re.sub(r'([a-zA-Z])\1{2,}',r'\1\1',s)

def preprocess(s):
  s = replace_urls(s)
  s = replace_emoticons(s)
  s = replace_names(s)
  s = replace_repeated(s)
  return s

if __name__ == '__main__':
  files_in = ['twitter_sentiment/train_pos.txt','twitter_sentiment/train_neg.txt','twitter_sentiment/test.txt']
  for fname in files_in:
    fname_out = fname+'.out'
    fin  = open(fname,'r')
    fout = open(fname_out,'w+')
    for line in fin:
      line_out = preprocess(line)
      fout.write(line_out)
    fin.close()
    fout.close()



